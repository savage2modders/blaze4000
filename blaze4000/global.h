#ifndef global_h
#define global_h

#define NOCONSOLE

#define _CRT_SECURE_NO_WARNINGS
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <stdio.h>
#include <string>

#ifdef NOCONSOLE
#define AllocConsole() ((void*)0)
#define FreeConsole() ((void*)0)
#define printf(x,...) ((void*)0)
#endif

typedef unsigned int uint32_t;
typedef unsigned long ulong32_t;
typedef unsigned char uint8_t;

#endif
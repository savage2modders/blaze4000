#ifndef mem_h
#define mem_h

#include "global.h"

namespace mem
{
    uint32_t GetModule(LPTSTR sModule)
    {
        return (uint32_t)GetModuleHandle(sModule);
    }
    uint32_t GetAddress(LPTSTR sModule, LPCSTR sProcedure)
    {
        HMODULE hMod = GetModuleHandle(sModule);
        if (!hMod)
        {
            return 0;
        }

        return (uint32_t)GetProcAddress(hMod, sProcedure);
    }

    bool WriteMem(uint32_t uAddress, uint8_t* bytes, uint32_t uLen)
    {
        ulong32_t prot;
        if (!VirtualProtect((LPVOID)uAddress, uLen, PAGE_EXECUTE_READWRITE, &prot))
        {
            return false;
        }
        memcpy((LPVOID)uAddress, bytes, uLen);
        VirtualProtect((LPVOID)uAddress, uLen, prot, &prot);
        return true;
    }
    bool WriteJmp(uint32_t uFrom, uint32_t uTo)
    {
        uint8_t patch[5] = { 0xE9, 0,0,0,0 };
        *(uint32_t*)&patch[1] = uTo - (uFrom + 5);
        return WriteMem(uFrom, patch, 5);
    }

    bool Hook(uint32_t uTarget, uint32_t uHookFunc, uint32_t* puOrig, uint32_t uPrologueLen = 5)
    {
        uint8_t* origBytes = (uint8_t*)VirtualAlloc(NULL, uPrologueLen+5, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
        if (!origBytes)
        {
            return false;
        }
        memcpy(origBytes, (LPVOID)uTarget, uPrologueLen);
        WriteJmp((uint32_t)&origBytes[uPrologueLen], uTarget+uPrologueLen);
        *puOrig = (uint32_t)origBytes;

        return WriteJmp(uTarget, uHookFunc);
    }

    bool VTableHook(void* pObj, uint32_t uIndex, uint32_t uHookFunc, uint32_t* puOrig)
    {
        uint32_t* vtable = *(uint32_t**)pObj;

        if (puOrig != NULL)
        {
            *puOrig = vtable[uIndex];
        }

        return WriteMem((uint32_t)&vtable[uIndex], (uint8_t*)&uHookFunc, sizeof(uint32_t));
    }
};

#endif
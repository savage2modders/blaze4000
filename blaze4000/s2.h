#ifndef s2_h
#define s2_h

#include "global.h"

#define MAX_STATES 32

/* game_shared */
const uint32_t offset_SetPersistantItemData = 0xC92A0;
const uint32_t offset_GiveInventoryItem = 0x6F790;

/* k2 */
//has a name: ?GetFloat@ICvar@@SAMABV?$basic_string@DU?$char_traits@D@std@@V?$allocator@D@2@@std@@@Z
//const uint32_t offset_ICvarGetFloat = 0x208950;

class ICvar
{

};

typedef float(__cdecl* tICvar__GetFloat)(const std::string&);
tICvar__GetFloat ICvar__GetFloat;

typedef int(__cdecl* tICvar__GetInteger)(const std::string&);
tICvar__GetInteger ICvar__GetInteger;

typedef ICvar*(__cdecl* tICvar__Find)(const std::string&);
tICvar__Find ICvar__Find;

template <typename T>
class s2Vec3
{
public:
    s2Vec3(T x, T y, T z) : m_X(x), m_Y(y), m_Z(z)
    { }
    s2Vec3() : m_x(), m_Y(), m_Z()
    { }

    T m_X, m_Y, m_Z;
};

class s2Entity
{
public:
    /* 0x0000 */ char unk0000[0x014 - 0x000];
    /* 0x0014 */ char m_Name[16];

    char* getName()
    {
        bool deref = false;
        for (int i = 0; m_Name[i] != '\0' && i < 16; i++)
        {
            if (m_Name[i] < 0x20 || m_Name[i] > 0x7E)
            {
                deref = true;
                break;
            }
        }
        return deref ? (*(char**)m_Name) : m_Name;
    }
};

struct s2VisualEntity
{
    /* 0x0000 */ char unk0000[0x118 - 0x000];
    /* 0x0118 */ s2Entity* m_States[MAX_STATES];
};

/* State_PersistantItem entity */
struct s2Persistant
{
    /* 0x0000 */ char unk0000[0x070 - 0x000];
    /* 0x0070 */ s2Vec3<float> m_StatsC;                    // C weird.. scale?
    /* 0x007C */ s2Vec3<float> m_StatsMoveSpeed;            // 6
    /* 0x0088 */ char unk0088[0x094 - 0x088];               
    /* 0x0094 */ s2Vec3<float> m_StatsDamage;               // 8 damage?
    /* 0x00A0 */ char unk00A0[0x0B8 - 0x0A0];
    /* 0x00B8 */ s2Vec3<float> m_StatsMaxHealth;            // 5
    /* 0x00C4 */ s2Vec3<float> m_StatsMaxMana;              // 1
    /* 0x00D0 */ s2Vec3<float> m_StatsMaxStamina;           // 7
    /* 0x00DC */ s2Vec3<float> m_StatsHealthRegen;          // 10
    /* 0x00E8 */ s2Vec3<float> m_StatsManaRegen;            // 12
    /* 0x00F4 */ s2Vec3<float> m_StatsStaminaRegen;         // 13
    /* 0x0100 */ s2Vec3<float> m_StatsMaxArmor;             // 4
    /* 0x010C */ char unk010C[0x118 - 0x10C];
    /* 0x0118 */ s2Vec3<float> m_StatsDamage2;              // 9 also damage???
    /* 0x0124 */ s2Vec3<float> m_StatsA;                    // A couldnt see any change
    /* 0x0130 */ s2Vec3<float> m_StatsReceivedDamage;       // B received damage
    /* 0x013C */ char unk013C[0x154 - 0x13C];
    /* 0x0154 */ s2Vec3<float> m_StatsMaxIncome;            // 2
    /* 0x0160 */ s2Vec3<float> m_StatsYellowRegen;          // 11
    /* 0x016C */ char unk016C[0x18C - 0x16C];

    /* Standard encoding seen here http://forums.savage2.com/showpost.php?p=144308&postcount=3 */
    /* 0x018C */ uint32_t m_Id;

    /* It's m_Id with digits seperated in the order:
        Type[4] = { digit 2, digit 3, digit 4, digit 1 }
        i.e. +19C is first digit (ring/amulet/jewel/rune),
        +190 is the second digit:
            1 = Red
            2 = Yellow (Note this one does nothing, the functionality was removed)
            3 = Blue
            4 = White
        +194 is the third digit:
            1 = Dolphin
            2 = Beaver
            3 = Fox (Not this one does nothing)
            4 = Armadillo 
            5 = Bear
            6 = Cheetah (Note this one does nothing)
            7 = Rabbit
        +198 is the last digit (replenish element):
            1 = Lung
            2 = Heart
            3 = Shield 
            4 = Boots
            5 = Brain
            6 = Dagger

        relevant stat vectors:
            stat type[1] (+194)
            stat type[0]+15 (+190)
    */
    /* 0x0190 */ uint32_t m_Type[4];
};

#define S2GAMESIG "?s_pGame@IGame@@0PAV1@A"
class s2Game
{
public:
    virtual void vFunc000();
    virtual void vFunc001();
    virtual void vFunc002();
    virtual void vFunc003();
    virtual void vFunc004();
    virtual void vFunc005();
    virtual void vFunc006();
    virtual void vFunc007();
    virtual s2Entity* vAllocateEntity(uint32_t id, uint32_t ownerId);
};

#endif
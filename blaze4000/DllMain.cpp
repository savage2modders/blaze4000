#include "global.h"
#include "mem.h"
#include "s2.h"

void blaze4000_init(void);
void blaze4000_hook(void);

s2Game* s_pGame = NULL;

//already defined in stddef.h :O
//#define offsetof(s,m) (int)&(((s *)0)->m)
#define elements(s) (sizeof(s)/sizeof(s[0]))
#define	FLOATP_OFFSET(dat,offset) ((float*)((char*)(dat) + offset))
#define ShowMsg(msg) MessageBoxA(NULL, msg, "Message", MB_OK)

struct StateModifiers
{
    const char* suffix;
    uint32_t offset;
};
StateModifiers g_StateMods[] = {
    { "_State_MoveSpeedMult", offsetof(s2Persistant, m_StatsMoveSpeed.m_Y) },
    { "_State_DamageMult", offsetof(s2Persistant, m_StatsDamage.m_Y) },
    { "_State_MaxHealthMult", offsetof(s2Persistant, m_StatsMaxHealth.m_Y) },
    { "_State_MaxManaMult", offsetof(s2Persistant, m_StatsMaxMana.m_Y) },
    { "_State_MaxStaminaMult", offsetof(s2Persistant, m_StatsMaxStamina.m_Y) },
    { "_State_HealthRegenMult", offsetof(s2Persistant, m_StatsHealthRegen.m_Y) },
    { "_State_ManaRegenMult", offsetof(s2Persistant, m_StatsManaRegen.m_Y) },
    { "_State_StaminaRegenMult", offsetof(s2Persistant, m_StatsStaminaRegen.m_Y) },
    { "_State_MaxArmorMult", offsetof(s2Persistant, m_StatsMaxArmor.m_Y) },
    { "_State_DamageMult2", offsetof(s2Persistant, m_StatsDamage2.m_Y) },
    { "_State_ReceivedDamageMult", offsetof(s2Persistant, m_StatsReceivedDamage.m_Y) },
    { "_State_MaxIncomeMult", offsetof(s2Persistant, m_StatsMaxIncome.m_Y) }
};

uint32_t _oSetPersistantItemData = 0;
void __declspec(naked) __stdcall oSetPersistantItemData(s2Persistant* pPersistantItem)
{
    __asm
    {
        mov edi, [esp+4]
        mov eax, [_oSetPersistantItemData]
        call eax
        retn 4
    };
}
void __stdcall hkSetPersistantItemData(s2Persistant* pPersistantItem)
{
    printf("hkPersistantItemData(0x%p) called.\r\n", pPersistantItem);
    oSetPersistantItemData(pPersistantItem);
    //pPersistantItem->m_StatsMaxHealth.m_X = 1.15f;
    //pPersistantItem->m_StatsMaxHealth.m_Z = 1.50f;
}
void __declspec(naked) __stdcall _hkSetPersistantItemData(void)
{
    __asm
    {
        push edi
        call hkSetPersistantItemData
        retn
    }
}

s2Entity* g_LastItemGiven = NULL;
s2Persistant* g_LastPersistantState = NULL;
void UpdatePersistantState(void)
{
    char* itemName = g_LastItemGiven->getName();
    printf("%s gave persistant state 0x%p\r\n", itemName, g_LastPersistantState);
    for (int i = 0; i < (int)elements(g_StateMods); i++)
    {
        if (ICvar__Find(itemName + std::string(g_StateMods[i].suffix)) != NULL)
        {
            float val = ICvar__GetFloat(itemName + std::string(g_StateMods[i].suffix));
            printf("%s -> %.2f\r\n", g_StateMods[i].suffix, val);
            *FLOATP_OFFSET(g_LastPersistantState, g_StateMods[i].offset) = val;
        }
        //else {
        //    printf("Could not find %s\r\n", (itemName + std::string(g_StateMods[i].suffix)).c_str());
        //}
    }
    if (ICvar__Find(itemName + std::string("_State_Type")) != NULL)
    {
        int iType = ICvar__GetInteger(itemName + std::string("_State_Type"));
        printf("StateType: %d\r\n", iType);
        g_LastPersistantState->m_Id = iType;
        g_LastPersistantState->m_Type[3] = iType % 10;
        g_LastPersistantState->m_Type[2] = (iType / 1000) % 10;
        g_LastPersistantState->m_Type[1] = (iType / 100) % 10;
        g_LastPersistantState->m_Type[0] = (iType / 10) % 10;
    }
    g_LastItemGiven = NULL;
    g_LastPersistantState = NULL;
}

typedef bool(__thiscall* tGiveInventoryItem)(s2Entity*);
tGiveInventoryItem oGiveInventoryItem;
bool __fastcall hkGiveInventoryItem(s2Entity* pInventoryItem)
{
    g_LastItemGiven = pInventoryItem;
    char* itemName = pInventoryItem->getName();
    printf("[0x%p] hkGiveInventoryItem %s\r\n", pInventoryItem, itemName);
    if (g_LastPersistantState != NULL)
    {
        UpdatePersistantState();
    }
    return oGiveInventoryItem(pInventoryItem);
}

/*
[0x02D66C08] hkvAllocateEntity(0x2C5, 0xFFFFFFFF) called.
[0x02D66C08] hkvAllocateEntity(0x57A, 0x943) called.
[0x02D66C08] hkvAllocateEntity(0x57B, 0x943) called.
[0x02D66C08] hkvAllocateEntity(0x57C, 0x943) called.
[0x02D66C08] hkvAllocateEntity(0x46F, 0x943) called.
[0x02D66C08] hkvAllocateEntity(0x1802CB, 0xFFFFFFFF) called.
[0x02D66C08] hkvAllocateEntity(0x23560203, 0x948) called.
[0x02D66C08] hkvAllocateEntity(0x234B01A2, 0x948) called.
[0x02D66C08] hkvAllocateEntity(0x234A0198, 0x948) called.
[0x02D66C08] hkvAllocateEntity(0x235D03ED, 0x948) called.
[0x02D66C08] hkvAllocateEntity(0x235E03F1, 0x948) called.
[0x02D66C08] hkvAllocateEntity(0x23690467, 0x948) called.
[0x02D66C08] hkvAllocateEntity(0x235E0404, 0x948) called.
[0x02D66C08] hkvAllocateEntity(0x57A, 0x948) called.
[0x02D66C08] hkvAllocateEntity(0x57B, 0x948) called.
[0x02D66C08] hkvAllocateEntity(0x57C, 0x948) called.
[0x02D66C08] hkvAllocateEntity(0x46F, 0x948) called.
*/
typedef s2Entity*(__thiscall* tvAllocateEntity)(s2Game*, uint32_t, uint32_t);
tvAllocateEntity ovAllocateEntity;
s2Entity* __fastcall hkvAllocateEntity(s2Game* pGame, void* EDX, uint32_t id, uint32_t entityId)
{
    printf("[0x%p] hkvAllocateEntity(0x%X, 0x%X) called.\r\n", pGame, id, entityId);
    s2Entity* pEnt = ovAllocateEntity(pGame, id, entityId);
    g_LastPersistantState = NULL;
    if (pEnt != NULL)
    {
        printf("\t->%s\r\n", pEnt->getName());
        if (strcmp(pEnt->getName(), "State_PersistantItem")==0)
        {
            g_LastPersistantState = (s2Persistant*)pEnt;
            printf("g_LastPersistantState 0x%p\r\n", g_LastPersistantState);
            if (g_LastItemGiven != NULL)
            {
                UpdatePersistantState();
            }
            /*if (g_LastItemGiven != NULL)
            {
                char* itemName = g_LastItemGiven->getName();
                printf("%s gave persistant state 0x%p\r\n", itemName, g_LastPersistantState);
                for (int i = 0; i < (int)elements(g_StateMods); i++)
                {
                    if (ICvar__Find(itemName + std::string(g_StateMods[i].suffix)) != NULL)
                    {
                        float val = ICvar__GetFloat(itemName + std::string(g_StateMods[i].suffix));
                        printf("%s -> %.2f", g_StateMods[i].suffix, val);
                        *FLOATP_OFFSET(g_LastPersistantState, g_StateMods[i].offset) = val;
                    }
                }
                g_LastItemGiven = NULL;
            }*/
        }
    }
    return pEnt;
}

void blaze4000_hook(void)
{
    uint32_t game_shared = mem::GetModule(L"game_shared");
    while (!game_shared)
    {
        game_shared = mem::GetModule(L"game_shared");
    }
    printf("game_shared 0x%p\r\n", game_shared);
    while (s_pGame == NULL)
    {
        s_pGame = *(s2Game**)mem::GetAddress(L"game_shared", S2GAMESIG);
    }
    printf("s_pGame 0x%p\r\n", s_pGame);

    mem::VTableHook(s_pGame, 0x20 / 4, (uint32_t)&hkvAllocateEntity, (uint32_t*)&ovAllocateEntity);
    printf("Hooked s2Game::vAllocateEntity.\r\n");

    uint32_t addr_GiveInventoryItem = game_shared + offset_GiveInventoryItem;
    printf("addr_GiveInventoryItem 0x%p\r\n", addr_GiveInventoryItem);
    mem::Hook(addr_GiveInventoryItem, (uint32_t)&hkGiveInventoryItem, (uint32_t*)&oGiveInventoryItem, 7);
    printf("Hooked GiveInventoryItem\r\n"); 

    /*uint32_t addr_SetPersistantItemData = game_shared + offset_SetPersistantItemData;
    printf("addr_SetPersistantItemData 0x%p\r\n", addr_SetPersistantItemData);
    mem::Hook(addr_SetPersistantItemData, (uint32_t)&_hkSetPersistantItemData, &_oSetPersistantItemData, 7);
    printf("Hooked SetPersistantItemData\r\n");*/

}

void blaze4000_thread(void)
{
    ShowMsg("blaze4000 #1.2.1");
    s_pGame = *(s2Game**)mem::GetAddress(L"game_shared", S2GAMESIG);
    printf("s_pGame 0x%p\r\n", s_pGame);

    ICvar__GetFloat = (tICvar__GetFloat)
        mem::GetAddress(L"k2", "?GetFloat@ICvar@@SAMABV?$basic_string@DU?$char_traits@D@std@@V?$allocator@D@2@@std@@@Z");
    printf("ICvar_GetFloat 0x%p\r\n", ICvar__GetFloat);
    printf("g_taxRate %.2f\r\n", ICvar__GetFloat("g_taxRate"));

    ICvar__GetInteger = (tICvar__GetInteger)
        mem::GetAddress(L"k2", "?GetInteger@ICvar@@SAHABV?$basic_string@DU?$char_traits@D@std@@V?$allocator@D@2@@std@@@Z");
    printf("ICvar_GetInteger 0x%p\r\n", ICvar__GetInteger);

    ICvar__Find = (tICvar__Find)
        mem::GetAddress(L"k2", "?Find@ICvar@@SAPAV1@ABV?$basic_string@DU?$char_traits@D@std@@V?$allocator@D@2@@std@@@Z");
    printf("ICvar__Find 0x%p\r\n", ICvar__Find);
    printf("g_taxRate ICvar* 0x%p\r\n", ICvar__Find("g_taxRate"));

    blaze4000_hook();
    while(!GetAsyncKeyState(VK_F12)&1)
    {
        Sleep(1000);
    }
    printf("Unhooking..\r\n");

    uint32_t game_shared = mem::GetModule(L"game_shared");
    mem::VTableHook(s_pGame, 0x20 / 4, (uint32_t)ovAllocateEntity, NULL);
    uint32_t addr_GiveInventoryItem = game_shared + offset_GiveInventoryItem;
    mem::WriteMem(addr_GiveInventoryItem, (uint8_t*)oGiveInventoryItem, 7);
    //uint32_t addr_SetPersistantItemData = game_shared + offset_SetPersistantItemData;
    //mem::WriteMem(addr_SetPersistantItemData, (uint8_t*)_oSetPersistantItemData, 7);
    printf("Bye!\r\n");
    FreeConsole();
}

void blaze4000_init(void)
{
    ShowMsg("blaze4000 #1.1");
    AllocConsole();
    freopen("CONOUT$", "w", stdout);
    setvbuf(stdout, NULL, _IONBF, NULL);
    printf("blaze4000_init()\r\n");
    ShowMsg("blaze4000 #1.2");

    CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)blaze4000_thread, NULL, 0, NULL);
    ShowMsg("blaze4000 #1.3");
}

int __stdcall DllMain(HMODULE hMod, DWORD reason, LPVOID lpReserved)
{
    switch (reason)
    {
    case DLL_PROCESS_ATTACH:
        {
            ShowMsg("blaze4000 #1");
            blaze4000_init();
            ShowMsg("blaze4000 #2");
        } break;
    }
    return TRUE;
}